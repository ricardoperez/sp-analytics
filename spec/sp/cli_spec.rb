RSpec.describe Sp::Analytics::CLI do
  subject(:cli_obj) { described_class.new(argv) }

  describe "#process" do
    subject(:process) { cli_obj.process }

    context "when only file path is set as argument" do
      let(:access_log_path) { "spec/test_logs/webserver.log" }
      let(:argv) { [access_log_path] }
      let(:expected_result) do
        <<~OUTPUT.strip
          == PAGE VIEWS
          /about/2 90 visits
          /contact 89 visits
          /index 82 visits
          /about 81 visits
          /help_page/1 80 visits
          /home 78 visits

          == UNIQUE VIEWS
          /contact 23 unique views
          /help_page/1 23 unique views
          /home 23 unique views
          /index 23 unique views
          /about/2 22 unique views
          /about 21 unique views
        OUTPUT
      end

      it "list page views and unique_views in order" do
        expect(process).to eq(expected_result)
      end
    end

    context "when the option --client-ips" do
      let(:argv) { ["spec/test_logs/webserver_small.log", "--client-ips"] }
      let(:expected_result) do
        <<~OUTPUT.strip
          316.433.849.80 4 client access
          316.433.849.805 2 client access
          444.701.448.104 2 client access
          235.313.352.950 1 client access
          543.910.244.929 1 client access
          646.865.545.408 1 client access
          929.398.951.889 1 client access
        OUTPUT
      end

      it "list client ips access" do
        expect(process).to eq(expected_result)
      end
    end

    context "when missing arguments" do
      let(:argv) { [] }

      it "list client ips access" do
        expect(process).to include("You need to pass the file path")
      end
    end

    context "when file was not found" do
      let(:file_path) { "wrong_file_path" }
      let(:argv) { [file_path] }

      it "list client ips access" do
        expect(process).to include("File #{file_path} not found")
      end
    end
  end
end
