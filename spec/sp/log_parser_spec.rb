RSpec.describe Sp::Analytics::LogParser do
  let(:access_log_path) { "spec/test_logs/webserver.log" }
  subject(:log_parser) { described_class.new(access_log_path) }

  describe "#page_views" do
    let(:expected_result) do
      <<~RESULT.strip
        /about/2 90 visits
        /contact 89 visits
        /index 82 visits
        /about 81 visits
        /help_page/1 80 visits
        /home 78 visits
      RESULT
    end

    subject(:page_views) { log_parser.page_views }

    it "list pages views in order" do
      expect(page_views).to eq(expected_result)
    end
  end

  describe "#unique_page_views" do
    let(:expected_result) do
      <<~RESULT.strip
        /contact 23 unique views
        /help_page/1 23 unique views
        /home 23 unique views
        /index 23 unique views
        /about/2 22 unique views
        /about 21 unique views
      RESULT
    end

    subject(:unique_page_views) { log_parser.unique_page_views }

    it "list unique views in order" do
      expect(unique_page_views).to eq(expected_result)
    end
  end

  describe "#client_access" do
    let(:access_log_path) { "spec/test_logs/webserver_small.log" }

    let(:expected_result) do
      <<~RESULT.strip
        316.433.849.80 4 client access
        316.433.849.805 2 client access
        444.701.448.104 2 client access
        235.313.352.950 1 client access
        543.910.244.929 1 client access
        646.865.545.408 1 client access
        929.398.951.889 1 client access
      RESULT
    end

    subject(:clients_access) { log_parser.clients_access }

    it "list client access in order" do
      expect(clients_access).to eq(expected_result)
    end
  end
end
