RSpec.describe Sp::Analytics do

  it "has a version number" do
    expect(Sp::Analytics::VERSION).not_to be nil
  end

  describe ".cli" do
    subject(:cli) { described_class.cli }

    context "when only filepath is given" do
      let(:arguments) { ["spec/test_logs/webserver_small.log"] }
      let(:expected_result) do
        <<~OUTPUT.strip
          == PAGE VIEWS
          /help_page/1 4 visits
          /home 3 visits
          /contact 2 visits
          /index 2 visits
          /about/2 1 visits

          == UNIQUE VIEWS
          /help_page/1 4 unique views
          /home 3 unique views
          /index 2 unique views
          /about/2 1 unique views
          /contact 1 unique views
        OUTPUT
      end

      it "list pages/paths views" do
        stub_const("ARGV", arguments)
        expect(cli).to eq(expected_result)
      end
    end

    context "when the option --client-ips" do
      let(:arguments) { ["spec/test_logs/webserver_small.log", "--client-ips"] }
      let(:expected_result) do
        <<~OUTPUT.strip
          316.433.849.80 4 client access
          316.433.849.805 2 client access
          444.701.448.104 2 client access
          235.313.352.950 1 client access
          543.910.244.929 1 client access
          646.865.545.408 1 client access
          929.398.951.889 1 client access
        OUTPUT
      end

      it "list client ips access" do
        stub_const("ARGV", arguments)
        expect(cli).to eq(expected_result)
      end
    end

  end
end
