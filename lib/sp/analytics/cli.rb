module Sp
  module Analytics
    class CLI
      TERMINAL_RED = "\e[31m"
      RESET_COLOR = "\e[0m"

      def initialize(argv)
        @file_path, @option = argv
        @argv = argv
      end

      def self.process(args)
        new(args).process
      end

      def process
        check_number_of_arguments
        parse_cli_arguments
      rescue ArgumentError => e
        error_message(e.message)
      rescue Errno::ENOENT
        error_message("File #{file_path} not found")
      end

      private

      attr_reader :file_path, :option, :argv

      def log
        @log ||= Sp::Analytics::LogParser.new(file_path)
      end

      def show_access
        <<~OUTPUT.strip
          == PAGE VIEWS
          #{log.page_views}

          == UNIQUE VIEWS
          #{log.unique_page_views}
        OUTPUT
      end

      def clients_ips_access
        log.clients_access
      end

      def check_number_of_arguments
        raise ArgumentError, 'You need to pass the file path' if no_parameters?
      end

      def no_parameters?
        argv.count.zero?
      end

      def error_message(msg)
        "#{TERMINAL_RED}#{msg}#{RESET_COLOR}"
      end

      def parse_cli_arguments
        if option == "--client-ips"
          clients_ips_access
        else
          show_access
        end
      end
    end
  end
end
