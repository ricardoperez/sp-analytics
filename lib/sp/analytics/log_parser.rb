module Sp
  module Analytics
    class LogParser

      def initialize(file_path)
        @access_log_lines = File.readlines(file_path)
      end

      def page_views
        @page_views ||= begin
          views_result = count_access(access_log)
          ordered_results(views_result, "visits")
        end
      end

      def unique_page_views
        @page_unique_views ||= begin
          views_result = count_access(access_log.uniq)
          ordered_results(views_result, "unique views")
        end
      end

      def clients_access
        @client_access ||= begin
          views_result = count_access(access_log, :client_ip)
          ordered_results(views_result, "client access")
        end
      end

      private

      attr_reader :access_log_lines

      def access_log
        @access_log ||=
          access_log_lines.map do |line|
            url_path, client_ip = line.split(' ')

            { path: url_path, client_ip: client_ip }
          end
      end

      def count_access(log, type = :path)
        views_counts = Hash.new(0)

        log.each do |log_line|
          path = log_line[type]
          views_counts[path] += 1
        end

        views_counts
      end

      def ordered_results(results, msg)
        results
        .sort_by { |path, views| [-views, path] }
        .map { |item, views| "#{item} #{views} #{msg}" }
        .join("\n")
      end
    end
  end
end
