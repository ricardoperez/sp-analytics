require "sp/analytics/version"
require "sp/analytics/log_parser"
require "sp/analytics/cli"

module Sp
  module Analytics
    def self.cli
      CLI.process(ARGV)
    end
  end
end
