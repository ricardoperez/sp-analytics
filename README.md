# Sp::Analytics

Ruby Test

## Run with docker

You can use the docker image, from this gitlab project, to run the parser

```
docker run -it -v FULL_PATH_TO_THE_LOG_FOLDER_HOST_MACHINE:/logs registry.gitlab.com/ricardoperez/sp-analytics:master bin/parser /logs/webserver.log
```

## Usage(withtout docker)

If you are checking out the repository, first you need to run `bin/setup` to install the dependencies.

Then, to run the parser to get page views and unique views
```
$ exe/parser logs/webserver.log
== PAGE VIEWS
/about/2 90 visits
/contact 89 visits
...
```

To get client ips access.
```
$ exe/parser logs/webserver.log --client-ips
158.577.775.616 31 client access
722.247.931.582 31 client access
184.123.665.067 29 client access
451.106.204.921 29 client access
```

## Running test 

```
$ bundle exec rspec
```

You can also check code coverage in `coverage` folder and [the CI](https://gitlab.com/ricardoperez/sp-analytics/-/jobs). 

