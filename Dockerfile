FROM ruby:2.7.1-alpine AS build

ARG APP_ROOT=/app

WORKDIR $APP_ROOT

RUN apk update \
    && apk upgrade \
    && apk add --update --no-cache build-base git

RUN gem install bundler -v 2.1

COPY Gemfile* ./
COPY . .

RUN bundle config --global frozen 1 \
    && bundle install --path=vendor/bundle

RUN gem build sp-analytics.gemspec --output=sp-analytics.gem
RUN gem install sp-analytics.gem

# run build
FROM ruby:2.7.1-alpine

COPY --from=build $GEM_HOME $GEM_HOME

WORKDIR $GEM_HOME
